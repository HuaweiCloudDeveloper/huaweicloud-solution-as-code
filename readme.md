## 仓库简介

全球数字经济提速，站在数字化转型风口，上云已成为千行百业实现转型跃升的新风标。然而，目前众多企业想上云但不懂云。为帮助企业高效上云，华为云推出Solution as Code，萃取丰富上云成功实践，提供一系列基于华为云可快速部署的解决方案，帮助用户降低上云门槛。同时开放完整源码，支持个性化配置，解决方案开箱即用，所见即所得。

## 项目总览

| 项目                            | 简介                             | 仓库                                                         |
| ------------------------------- | -------------------------------- | ------------------------------------------------------------ |
| CDN下载加速解决方案             | 静态资源下载加速                 | [huaweicloud-solution-cdn-download-acceleration](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-cdn-download-acceleration) |
| LInux服务器迁移上云             | 开箱即用的服务器迁移上云方案     | [huaweicloud-solution-migrating-Linux-servers-to-huawei-cloud](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-migrating-linux-servers-to-huawei-cloud) |
| 搭建跨境电商管理平台            | 云上快速搭建跨境店铺管理环境     | [huaweicloud-solution-E-commerce-store-access-environment](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-e-commerce-store-access-environment) |
| 电商秒杀大促数据库解决方案      | 解决海量用户访问数据库难题       | [huaweicloud-solution-E-commerce-seckill-big-promotion-database](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-e-commerce-seckill-big-promotion-database) |
| 高可用网站架构云化              | 快速搭建云上高可用网站架构       | [huaweicloud-solution-build-high-availability-four-layer-load-balancing](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-high-availability-four-layer-load-balancing) |
| 核心数据库上云                  | 一键部署云上核心数据库基础环境   | [huaweicloud-solution-oracle-rac-in-cloud](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-oracle-rac-in-cloud) |
| 基于Blender构建云端渲染服务     | 云上快速部署Blender软件          | [huaweicloud-solution-cloud-rendering-based-blender](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-cloud-rendering-based-blender) |
| 基于Discuz快速搭建论坛          | 云上快速部署Discuz论坛           | [huaweicloud-solution-build-a-forum-based-on-discuz](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-a-forum-based-on-discuz) |
| 基于Tomcat快速搭建Java web 环境 | 云上快速部署Java Web开发环境     | [huaweicloud-solution-build-a-java-web-environment-based-on-tomcat](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-a-java-web-environment-based-on-tomcat) |
| 基于WordPress搭建个人网站       | 云上快速部署WordPress的个人网站  | [huaweicloud-solution-build-a-personal-website-based-on-wordpress](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-a-personal-website-based-on-wordpress) |
| 基于云搜索服务的SQL加速         | 开箱即用的SQL加速方案            | [huaweicloud-solution-search-acceleration-based-CSS](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-search-acceleration-based-css) |
| 快速搭建FTP战点                 | 云上快速部署VSFTPD软件           | [huaweicloud-solution-build-an-ftp-server](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-an-ftp-server) |
| 快速构建高可用四层负载均衡      | 云上快速部署LVS四层负载均衡      | [huaweicloud-solution-build-high-availability-four-layer-load-balancing](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-high-availability-four-layer-load-balancing) |
| 快速构建基因测序环境            | 云上快速部署Slurm基因测序环境    | [huaweicloud-solution-build-gene-sequencing-environment](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-build-gene-sequencing-environment) |
| 内容审核-图片审核               | 开箱即用的图片审核解决方案       | [huaweicloud-solution-moderation](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-moderation) |
| 全球数据传输加速                | 全球加速WSA服务动态加速网站访问  | [huaweicloud-solution-global-data-transfer-acceleration](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-global-data-transfer-acceleration) |
| 人证核身解决方案                | 开箱即用的人证核身解决方案       | [huaweicloud-solution-ID-verification](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-id-verification) |
| 文字识别OCR-发票报销            | 开箱即用的发票识别与验真解决方案 | [huaweicloud-solution-ocr](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-ocr) |
| 无服务器告警推送                | 开箱即用的自动推送告警信息平台   | [huaweicloud-solution-serverless-alert-notifier](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-serverless-alert-notifier) |
| 应用容器化上云                  | 快速帮助用户实现云原生容器化改造 | [huaweicloud-solution-application-containerization-to-the-cloud](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-application-containerization-to-the-cloud) |
| 云服务器一键备份                | 一键全量备份Region内云服务器     | [huaweicloud-solution-cloud-backup](https://gitee.com/HuaweiCloudDeveloper/huaweicloud-solution-cloud-backup) |

